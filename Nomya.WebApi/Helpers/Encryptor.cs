﻿using Nomya.WebApi.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Nomya.WebApi.Helpers
{
    public static class Encryptor
    {

        public static string Crypt(Account account, DateTime? expires)
        {
            byte[] identifiant = Encoding.ASCII.GetBytes($"{account.Username}*{account.Password}*{expires}*{account.RoleWeb}");

            byte[] key = Encoding.ASCII.GetBytes("28090C95678C41538CC30817D8649A6C");
            byte[] iv = Encoding.ASCII.GetBytes("Salutceciinomyaa");

            RijndaelManaged rijndael = new RijndaelManaged();
            rijndael.Mode = CipherMode.CBC;

            ICryptoTransform cTransform = rijndael.CreateEncryptor(key, iv);

            MemoryStream ms = new MemoryStream();

            CryptoStream cs = new CryptoStream(ms, cTransform, CryptoStreamMode.Write);

            cs.Write(identifiant, 0, identifiant.Length);
            cs.FlushFinalBlock();

            byte[] CypherBytes = ms.ToArray();

            ms.Close();
            cs.Close();
            string result = Convert.ToBase64String(CypherBytes);
            return result;
        }

        public static string Decrypt(string identifiant)
        {
            byte[] key = Encoding.ASCII.GetBytes("28090C95678C41538CC30817D8649A6C");
            byte[] iv = Encoding.ASCII.GetBytes("Salutceciinomyaa");

            var Dim = Convert.FromBase64String(identifiant);

            RijndaelManaged rijndael = new RijndaelManaged();

            rijndael.Mode = CipherMode.CBC;


            // Ecris les données déchiffrées dans le MemoryStream
            ICryptoTransform decryptor = rijndael.CreateDecryptor(key, iv);
            MemoryStream ms = new MemoryStream(Dim);
            CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read);

            // Place les données déchiffrées dans un tableau d'octet
            byte[] plainTextData = new byte[Dim.Length];

            int decryptedByteCount = cs.Read(plainTextData, 0, plainTextData.Length);

            ms.Close();
            cs.Close();

            string result = Encoding.ASCII.GetString(plainTextData, 0, decryptedByteCount);
            return result;

        }
    }
}
