﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nomya.WebApi.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
