﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nomya.WebApi.Entities
{
    public class News
    {
        [BsonId]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Img { get; set; }
        public string PostOn { get; set; }
        public string Author { get; set; }
        public List<Like> Like { get; set; }
    }
}
