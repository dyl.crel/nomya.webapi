﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Nomya.WebApi.Entities
{
    public class Account : Credential
    {
        [BsonId]
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string Birthdate { get; set; }
        public string Sponsorid { get; set; }
        public string Secretquestion { get; set; }
        public string Secretanswer { get; set; }
        public string Token { get; set; }
        public RolesEnum RoleWeb { get; set; }
        public GameHierarchyEnum Role { get; set; }
        public string Ticket {get;set;}
    }

    public enum RolesEnum
    { 
        User = 1,
        Moderator = 2,
        Administrator = 3
    }
    public enum GameHierarchyEnum
    {
        UNAVAILABLE = -1,
        PLAYER = 0,
        MODERATOR = 10,
        GAMEMASTER_PADAWAN = 20,
        GAMEMASTER = 30,
        ADMIN = 40,
        UNKNOW_SPECIAL_USER = 50
    }
}
