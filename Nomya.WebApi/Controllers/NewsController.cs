﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nomya.WebApi.Entities;
using Nomya.WebApi.Helpers;
using Nomya.WebApi.Services;

namespace Nomya.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NewsController : ControllerBase
    {
        private readonly INewsService _newsService;
        private readonly IAccountService _accountService;

        public NewsController(INewsService newsService, IAccountService accountService)
        {
            _newsService = newsService;
            _accountService = accountService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAll()
        {
           var news = _newsService.GetAll().OrderByDescending(c => DateTime.Parse(c.PostOn));

            return Ok(news);
        }

        [HttpGet("single/{id}")]
        [AllowAnonymous]
        public IActionResult GetById(int? id)
        {
            var singlenews = _newsService.Get(id.ToString());

            return Ok(singlenews);
        }

        [HttpPost("add")]
        [Authorize]
        public IActionResult PostNews([FromBody]News news)
        {
            string keyDecrypt = Encryptor.Decrypt(User.Claims.First(c => c.Type == "Session:ID").Value);

            string[] keyDecryptArray = keyDecrypt.Split('*');

            var username = keyDecryptArray[0];
            var password = keyDecryptArray[1];
            var Expires = keyDecryptArray[2];
            var Roles = keyDecryptArray[3];

            if (Roles != RolesEnum.Administrator.ToString() || DateTime.Parse(Expires) < DateTime.UtcNow)
                return Unauthorized();

             _newsService.PostNews(news, username);

            return Ok();
        }

        [HttpPost("like/{id}")]
        [Authorize]
        public IActionResult PostLike(int id)
        {
            string keyDecrypt = Encryptor.Decrypt(User.Claims.First(c => c.Type == "Session:ID").Value);

            string[] keyDecryptArray = keyDecrypt.Split('*');

            var username = keyDecryptArray[0];
            var password = keyDecryptArray[1];
            var Expires = keyDecryptArray[2];

            if (DateTime.Parse(Expires) < DateTime.UtcNow)
                return Unauthorized();


            var user = _accountService.Authenticate(username, password, false);

            if (user == null)
                return BadRequest("You're not logged");

            var singlenews = _newsService.PostLike(id, user.Username);

            return Ok(singlenews);
        }
    }
}