﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nomya.WebApi.Entities;
using Nomya.WebApi.Helpers;
using Nomya.WebApi.Services;

namespace Nomya.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AuthController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public IActionResult Login([FromBody]Credential credential)
        {
            var account = _accountService.Authenticate(credential.Username, credential.Password, true);

            if (account == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(account);
        }

        [HttpGet("autologin")]
        [Authorize]
        public IActionResult AutoLogin()
        {
            string keyDecrypt = Encryptor.Decrypt(User.Claims.First(c => c.Type == "Session:ID").Value);

            string[] keyDecryptArray = keyDecrypt.Split('*');

            var username = keyDecryptArray[0];
            var password = keyDecryptArray[1];
            var Expires = keyDecryptArray[2];

            if((DateTime.Parse(Expires) < DateTime.Now))
                return BadRequest("You're token is not valid");

            var user = _accountService.Authenticate(username, password, false);

            if (user == null)
                return BadRequest("404");

            return Ok(user);

        }
        [HttpPost("registration")]
        public IActionResult Register([FromBody]Account account)
        {
            var user = _accountService.Register(account);

            if (user == null)
                return BadRequest(new { message = "Username is existing" });

            return Ok(user);
        }

        [HttpPost("role")]
        public IActionResult GetRole([FromBody]Credential credential)
        {
            var account = _accountService.Authenticate(credential.Username, credential.Password, false);

            if (account == null)
                return BadRequest(new { message = "ByPass is not allowed" });

            return Ok(account.RoleWeb);
        }

        [HttpGet("autorole")]
        [Authorize]
        public IActionResult Autorole()
        {
            string keyDecrypt = Encryptor.Decrypt(User.Claims.First(c => c.Type == "Session:ID").Value);

            string[] keyDecryptArray = keyDecrypt.Split('*');

            var username = keyDecryptArray[0];
            var password = keyDecryptArray[1];
            var Expires = keyDecryptArray[2];

            if ((DateTime.Parse(Expires) < DateTime.Now))
                return BadRequest("You're token is not valid");

            var user = _accountService.Authenticate(username, password, false);

            if (user == null)
                return BadRequest("404");

            return Ok(user.RoleWeb);

        }
    }
}