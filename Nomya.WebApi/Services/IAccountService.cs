﻿using System.Collections.Generic;
using Nomya.WebApi.Entities;

namespace Nomya.WebApi.Services
{
    public interface IAccountService
    {
        Account Authenticate(string username, string password, bool isNeedToken);
        Account Register(Account account);
    }
}