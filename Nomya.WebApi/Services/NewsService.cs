﻿using MongoDB.Driver;
using Nomya.WebApi.Driver;
using Nomya.WebApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nomya.WebApi.Services
{
    public class NewsService : INewsService
    {
        private List<News> _news = Database.News.FindSync(FilterDefinition<News>.Empty).ToList();

        public IEnumerable<News> GetAll()
        {
            return _news;
        }

        public IEnumerable<News> Get(string id)
        {
            return _news.Where(c => c.Id.ToString() == id);
        }

        public IEnumerable<News> PostLike(int? id, string name)
        {
            if (id == null)
                return null;

            _news.ForEach(c =>
            {
                var username = c.Like.FirstOrDefault(f => f.Username == name);
                if (c.Id == id && username != null)
                {
                    c.Push(news =>
                    {
                        c.Like.Remove(username);
                    });
                }
                else
                {
                    if(c.Id == id)
                    {
                        c.Push(news =>
                        {
                            c.Like.Add(new Like() { Username = name });
                        });
                    }
                }
            });

            return _news.Where(c => c.Id == id);
        }

        public void PostNews(News news, string username)
        {
            var newsId = _news.Count == 0 ? 0 : _news.Count + 1;

            news.Id = newsId;
            news.Author = username;
            news.Like = new List<Like>();
            news.PostOn = DateTime.UtcNow.ToLongDateString();
            news.Insert();
        }
    }
}
