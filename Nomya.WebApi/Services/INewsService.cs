﻿using System.Collections.Generic;
using Nomya.WebApi.Entities;

namespace Nomya.WebApi.Services
{
    public interface INewsService
    {
        IEnumerable<News> GetAll();
        IEnumerable<News> Get(string id);
        IEnumerable<News> PostLike(int? id, string name);
        void PostNews(News news, string username);
    }
}