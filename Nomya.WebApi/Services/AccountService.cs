﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Nomya.WebApi.Entities;
using Nomya.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Security.Cryptography;
using Nomya.WebApi.Driver;
using MongoDB.Driver;

namespace Nomya.WebApi.Services
{
    public class AccountService : IAccountService
    {
        private List<Account> _users = Database.Accounts.FindSync(FilterDefinition<Account>.Empty).ToList();
        private readonly AppSettings _settings;

        public AccountService(IOptions<AppSettings> settings)
        {
            _settings = settings.Value;
        }

        public Account Authenticate(string username, string password,bool isNeedToken)
        {
            var account = _users.SingleOrDefault(a => a.Username == username && a.Password == password);

            if (account == null)
                return null;

            if (isNeedToken)
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("nomyathebest123456789");

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim("Username", account.Username),
                    new Claim("Session:ID", Encryptor.Crypt(account, DateTime.UtcNow.AddDays(1)).ToString()),
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                account.Token = tokenHandler.WriteToken(token);
            }

            account.Password = null;


            return account;
        }

        public Account Register(Account account)
        {
            var userId = _users.Count == 0 ? 0 : _users.Count;

            var user = _users.FirstOrDefault(c => c.Username.ToLower() == account.Username.ToLower());

            if (user != null)
                return null;

            account.Id = userId;
            account.Birthdate = account.Birthdate.Split(' ')[0];
            account.RoleWeb = RolesEnum.User;
            account.Role = GameHierarchyEnum.ADMIN;
            account.Nickname = "Nomya";
            account.Insert();

            return account;
        }
    }
}
