﻿using MongoDB.Driver;
using Nomya.WebApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nomya.WebApi.Driver
{
    public static class Database
    {
        private static readonly IMongoDatabase Base = new MongoClient().GetDatabase("Horizon");
        public static readonly IMongoCollection<Account> Accounts = Base.GetCollection<Account>("Accounts");
        public static readonly IMongoCollection<News> News = Base.GetCollection<News>("News");

        public static void Insert<TDocument>(this TDocument document)
        {
            switch (document)
            {
                case Account e:
                    Accounts.InsertOne(e);
                    break;
                case News e:
                    News.InsertOne(e);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public static void Delete<TDocument>(this TDocument document)
        {
            switch (document)
            {
                case Account e:
                    Accounts.DeleteOne(x => x.Id == e.Id);
                    break;
                case News e:
                    News.DeleteOne(x => x.Id == e.Id);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public static void Push<TDocument>(this TDocument document, Action<TDocument> action = null)
        {
            action?.Invoke(document);

            switch (document)
            {
                case Account e:
                    Accounts.ReplaceOne(x => x.Id == e.Id, e);
                    break;
                case News e:
                    News.ReplaceOne(x => x.Id == e.Id, e);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

    }
}
